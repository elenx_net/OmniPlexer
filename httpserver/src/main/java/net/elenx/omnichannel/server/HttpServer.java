package net.elenx.omnichannel.server;

import lombok.extern.slf4j.Slf4j;
import ratpack.handling.Context;
import ratpack.server.RatpackServer;

@Slf4j
public class HttpServer
{
    public static void main(String[] args) throws Exception
    {
        new HttpServer().runServer();
    }

    private void runServer() throws Exception
    {
        RatpackServer.start(server -> server
                .handlers(handler -> handler
                        .path("", context -> context
                                .byMethod(action -> action
                                        .get(this::responseGet)
                                        .post(this::responsePost))
                        )
                )
        );
    }

    private void responsePost(Context ctx)
    {
        log.info("Received POST request");
        ctx.render("[POST] Omni HttpServer responded correctly.");
    }

    private void responseGet(Context ctx)
    {
        log.info("Received GET request");
        ctx.render("[GET] Omni HttpServer responded correctly.");
    }
}
