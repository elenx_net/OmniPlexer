package net.elenx.omnichannel.network.dto.http;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

import java.util.List;
import java.util.Optional;

@Getter
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class PostRequest extends HttpRequest
{
    Object body;

    @Builder
    private PostRequest(String url, List<Header> headers, ChangeIpCondition changeIpCondition, Object body)
    {
        super(url, headers, changeIpCondition);
        this.body = body;
    }

    @Override
    public HttpMethod httpMethod()
    {
        return HttpMethod.POST;
    }

    @Override
    public Optional<Object> body()
    {
        return Optional.ofNullable(body);
    }

    @Override
    HttpRequest withNewHeaders(List<Header> newHeaders) {
        return new PostRequest(this.url, newHeaders, this.changeIpCondition, this.body);
    }
}
