package net.elenx.omnichannel.network.httpnetworkservice.webclient;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import net.elenx.omnichannel.network.dto.http.Header;
import net.elenx.omnichannel.network.dto.http.OmniHttpRequest;
import net.elenx.omnichannel.network.dto.http.OmniHttpResponse;
import net.elenx.omnichannel.network.httpnetworkservice.HttpNetworkService;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpMethod;
import org.springframework.web.reactive.function.BodyExtractors;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.io.InputStream;
import java.io.SequenceInputStream;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
public class WebClientNetworkService implements HttpNetworkService
{
    WebClient webClient;

    @Override
    public CompletableFuture<OmniHttpResponse> send(OmniHttpRequest request)
    {
        var requestBuilder = webClient
                .method(fetchMethod(request))
                .uri(request.url());

        for (Header header : request.headers())
        {
            requestBuilder.header(header.key(), header.value());
        }

        request.body().ifPresent(requestBuilder::bodyValue);

        return requestBuilder
                .exchangeToMono(this::toResponse)
                .doOnError(exc -> System.out.println("ERRORasdda"))
                .toFuture();
    }

    private Mono<OmniHttpResponse> toResponse(ClientResponse clientResponse)
    {
        return clientResponse
                .body(BodyExtractors.toDataBuffers())
                .map(DataBuffer::asInputStream)
                .collectList()
                .map(inputStreams -> createResponse(inputStreams, clientResponse));
    }

    private static OmniHttpResponse createResponse(List<InputStream> inputStreams, ClientResponse clientResponse)
    {
        var inputStream = new SequenceInputStream(Collections.enumeration(inputStreams));
        var body = new WebClientResponseBody(inputStream);

        var headers = clientResponse.headers()
                .asHttpHeaders()
                .entrySet()
                .stream()
                .map(header -> new Header(header.getKey(), header.getValue().toArray(String[]::new)))
                .toList();

        return new WebClientHttpResponse(clientResponse.statusCode().value(), body, headers);
    }

    private HttpMethod fetchMethod(OmniHttpRequest request)
    {
        return switch (request.httpMethod())
                {
                    case GET -> HttpMethod.GET;
                    case PUT -> HttpMethod.PUT;
                    case POST -> HttpMethod.POST;
                    case DELETE -> HttpMethod.DELETE;
                };
    }
}
