package net.elenx.omnichannel.network.httpnetworkservice.webclient;

import net.elenx.omnichannel.network.dto.Body;
import net.elenx.omnichannel.network.dto.http.Header;
import net.elenx.omnichannel.network.dto.http.OmniHttpResponse;

import java.util.List;

public record WebClientHttpResponse(int statusCode, Body body, List<Header> headers) implements OmniHttpResponse
{
}
