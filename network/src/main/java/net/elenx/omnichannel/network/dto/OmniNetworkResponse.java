package net.elenx.omnichannel.network.dto;

public interface OmniNetworkResponse
{
    Body body();
}
