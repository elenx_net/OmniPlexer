package net.elenx.omnichannel.network.httpnetworkservice.tor;

import io.micrometer.common.util.StringUtils;
import lombok.AccessLevel;
import lombok.SneakyThrows;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import net.elenx.omnichannel.network.httpnetworkservice.tor.configuration.TorNode;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@Slf4j
class TorControlPort
{
    private static final String OK_RESPONSE = "250 OK";
    String nodeInfo;

    String auth;
    Socket clientSocket;
    PrintWriter out;
    BufferedReader in;

    @SneakyThrows
    public TorControlPort(TorNode torNode)
    {
        this.nodeInfo = String.format("Host: %s Tor port: %s Tor Control Port: %s", torNode.host(), torNode.port(), torNode.controlPort());
        this.auth = StringUtils.isBlank(torNode.controlPortAuth()) ? "\"\"" : torNode.controlPortAuth();

        log.debug("Tor Control Port connecting. {}", nodeInfo);

        this.clientSocket = new Socket(torNode.host(), torNode.controlPort());
        this.out = new PrintWriter(clientSocket.getOutputStream());
        this.in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

        log.debug("Connected to Tor Control Port. {}", nodeInfo);
    }

    @SneakyThrows
    public void changeIp()
    {
        log.debug("Changing IP. {}", nodeInfo);

        out.println(("authenticate " + auth));
        out.flush();
        var response = in.readLine();
        if (!response.equals(OK_RESPONSE))
        {
            errorDisconnect("Cannot authenticate to TOR Control Port.", response);
        }

        out.println("signal newnym");
        out.flush();
        response = in.readLine();
        if (!response.equals(OK_RESPONSE))
        {
            errorDisconnect("Cannot send NEWNYM signal.", response);
        }

        log.debug("IP changed. {}", nodeInfo);
    }

    @SneakyThrows
    private void errorDisconnect(String errorMessage, String response)
    {
        log.error("Disconnecting due to: {}. Tor response: {}. {}", errorMessage, response, nodeInfo);

        out.println("quit");
        out.flush();
        var quitResponse = in.readLine();
        closeConnection();

        if (!response.equals("250 closing connection"))
        {
            log.warn("Cannot quit Control Port connection - forced close. Error: {}. {}", quitResponse, nodeInfo);
        }

        log.error("Disconnected. {}", nodeInfo);

        throw new IllegalStateException(errorMessage + "Disconnected. Error is: " + response);
    }

    @SneakyThrows
    public void closeConnection()
    {
        in.close();
        out.close();
        clientSocket.close();
    }
}
