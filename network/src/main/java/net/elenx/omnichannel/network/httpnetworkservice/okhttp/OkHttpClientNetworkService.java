package net.elenx.omnichannel.network.httpnetworkservice.okhttp;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import net.elenx.omnichannel.network.dto.http.Header;
import net.elenx.omnichannel.network.dto.http.OmniHttpRequest;
import net.elenx.omnichannel.network.dto.http.OmniHttpResponse;
import net.elenx.omnichannel.network.httpnetworkservice.HttpNetworkService;
import net.elenx.omnichannel.network.httpnetworkservice.webclient.WebClientHttpResponse;
import net.elenx.omnichannel.network.httpnetworkservice.webclient.WebClientResponseBody;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class OkHttpClientNetworkService implements HttpNetworkService {

    OkHttpClient httpClient;

    @Override
    public CompletableFuture<OmniHttpResponse> send(OmniHttpRequest request) {
        var re = new CompletableFuture<Response>();
        var builder = new Request.Builder()
                .url(request.url())
                .method(request.httpMethod().toString(), request.body().map(body -> RequestBody.create(body.toString(), null)).orElse(null));

        for (Header header : request.headers()) {
            for (String headerValue : header.value()) {
                builder = builder.addHeader(header.key(), headerValue);
            }
        }

        httpClient.newCall(builder.build()).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                re.completeExceptionally(e);
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) {
                re.complete(response);
            }
        });

        return re.thenApply(this::toOmniResponse);
    }


    private OmniHttpResponse toOmniResponse(Response receivedResponse) {
        var body = new WebClientResponseBody(receivedResponse.body().byteStream());
        return new WebClientHttpResponse(receivedResponse.code(), body, fetchHeaders(receivedResponse.headers()));
    }

    private List<Header> fetchHeaders(Headers headers) {
        return headers.toMultimap().entrySet()
                .stream()
                .map(entry -> new Header(entry.getKey(), entry.getValue().toArray(String[]::new)))
                .toList();
    }
}
