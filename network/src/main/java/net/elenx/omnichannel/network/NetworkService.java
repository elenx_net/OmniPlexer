package net.elenx.omnichannel.network;

import net.elenx.omnichannel.network.dto.http.OmniHttpRequest;
import net.elenx.omnichannel.network.dto.http.OmniHttpResponse;

import java.util.concurrent.CompletableFuture;

public interface NetworkService
{
    CompletableFuture<OmniHttpResponse> send(OmniHttpRequest request);
}
