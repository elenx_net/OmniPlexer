package net.elenx.omnichannel.network.httpnetworkservice.tor.configuration;

import net.elenx.omnichannel.network.httpnetworkservice.HttpNetworkService;

public record TorNetworkServiceNode(TorNode torNode, HttpNetworkService networkService)
{
}
