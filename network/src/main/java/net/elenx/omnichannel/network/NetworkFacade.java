package net.elenx.omnichannel.network;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import net.elenx.omnichannel.network.dto.http.OmniHttpRequest;
import net.elenx.omnichannel.network.dto.http.OmniHttpResponse;
import net.elenx.omnichannel.network.httpnetworkservice.HttpNetworkService;

import java.util.concurrent.CompletableFuture;

@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
class NetworkFacade implements NetworkService
{
    HttpNetworkService httpNetworkService;

    @Override
    public CompletableFuture<OmniHttpResponse> send(OmniHttpRequest request)
    {
        return httpNetworkService.send(request);
    }
}
