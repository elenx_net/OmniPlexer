package net.elenx.omnichannel.network.httpnetworkservice;

import net.elenx.omnichannel.network.dto.http.OmniHttpRequest;
import net.elenx.omnichannel.network.dto.http.OmniHttpResponse;

import java.io.Closeable;
import java.util.concurrent.CompletableFuture;

public interface HttpNetworkService extends Closeable
{
    CompletableFuture<OmniHttpResponse> send(OmniHttpRequest request);

    @Override
    default void close()
    {
    }
}
