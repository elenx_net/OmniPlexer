package net.elenx.omnichannel.network.httpnetworkservice.tor;

import io.netty.util.concurrent.DefaultThreadFactory;
import io.reactivex.rxjava3.core.Scheduler;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import io.reactivex.rxjava3.subjects.Subject;
import io.reactivex.rxjava3.subjects.UnicastSubject;
import lombok.AccessLevel;
import lombok.SneakyThrows;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import net.elenx.omnichannel.network.dto.http.OmniHttpRequest;
import net.elenx.omnichannel.network.dto.http.OmniHttpResponse;
import net.elenx.omnichannel.network.httpnetworkservice.HttpNetworkService;
import net.elenx.omnichannel.network.httpnetworkservice.tor.configuration.TorNode;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@Slf4j
class TorWorker implements HttpNetworkService {
    ExecutorService workerThread;

    Subject<AsyncRequest> subject;
    Disposable disposable;

    HttpNetworkService httpNetworkService;
    TorControlPort torControlPort;

    TimestampSampler sampler;

    public TorWorker(HttpNetworkService httpNetworkService, TorNode torNode) {
        this.httpNetworkService = httpNetworkService;
        this.torControlPort = new TorControlPort(torNode);
        sampler = new TimestampSampler();
        sampler.update();

        workerThread = Executors.newSingleThreadExecutor(new DefaultThreadFactory("tor-worker-" + torNode.host() + ":" + torNode.port()));
        Scheduler scheduler = Schedulers.from(workerThread);

        subject = UnicastSubject.<AsyncRequest>create().toSerialized();

        disposable = subject
                .doOnNext(this::handleRequest)
                .subscribeOn(scheduler)
                .subscribe();
    }

    private void handleRequest(AsyncRequest asyncRequest) {
        var omniRequest = asyncRequest.request;
        var requestStartTimestamp = TimestampSampler.now();

        this.httpNetworkService.send(omniRequest).handleAsync((response, throwable) ->
        {
                handleSuccess(asyncRequest, requestStartTimestamp, response, throwable);


            return null;
        }, workerThread);
    }

    private void handleSuccess(AsyncRequest asyncRequest, long requestStartTimestamp, OmniHttpResponse response, Throwable throwable) {
        if (asyncRequest.request.changeIpCondition().test(response) || throwable != null) {
            if (sampler.lastUpdateAfter(requestStartTimestamp)) {
                torControlPort.changeIp();
                sampler.update();
            }
            subject.onNext(asyncRequest);
        } else {
            asyncRequest.replyTo.complete(response);
        }
    }

    @Override
    public CompletableFuture<OmniHttpResponse> send(OmniHttpRequest request) {
        var response = new CompletableFuture<OmniHttpResponse>();
        subject.onNext(new AsyncRequest(request, response));
        return response;
    }

    @Override
    @SneakyThrows
    public void close() {
        workerThread.shutdownNow();
        var terminated = workerThread.awaitTermination(10, TimeUnit.SECONDS);

        disposable.dispose();
        torControlPort.closeConnection();

        if (!terminated)
            throw new IllegalStateException("Cannot close tor-worker pool");
    }

    private record AsyncRequest(OmniHttpRequest request, CompletableFuture<OmniHttpResponse> replyTo) {
    }
}
