package net.elenx.omnichannel.network.httpnetworkservice.webclient;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.experimental.FieldDefaults;
import net.elenx.omnichannel.network.dto.Body;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;

@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
public class WebClientResponseBody implements Body
{
    InputStream inputStream;

    @Override
    @SneakyThrows
    public String asText()
    {
        try (inputStream)
        {
            return new String(inputStream.readAllBytes(), StandardCharsets.UTF_8);
        }
    }

    @Override
    public InputStream asInputStream()
    {
        return inputStream;
    }

    @Override
    public <T> T convertTo(Class<T> typeClass)
    {
        throw new IllegalStateException("Not implemented");
    }
}
