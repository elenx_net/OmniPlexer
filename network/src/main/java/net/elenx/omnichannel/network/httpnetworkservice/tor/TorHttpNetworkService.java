package net.elenx.omnichannel.network.httpnetworkservice.tor;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import net.elenx.omnichannel.network.dto.http.Header;
import net.elenx.omnichannel.network.dto.http.HttpRequest;
import net.elenx.omnichannel.network.dto.http.OmniHttpRequest;
import net.elenx.omnichannel.network.dto.http.OmniHttpResponse;
import net.elenx.omnichannel.network.httpnetworkservice.HttpNetworkService;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicInteger;

@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
class TorHttpNetworkService implements HttpNetworkService {
    AtomicInteger index = new AtomicInteger();

    List<TorWorker> torWorkers;

    @Override
    public CompletableFuture<OmniHttpResponse> send(OmniHttpRequest request) {

        return torWorkers
                .get(index.getAndIncrement() % torWorkers.size())
                .send(randomUserAgentHeader(request));
    }

    private OmniHttpRequest randomUserAgentHeader(OmniHttpRequest request) {
        return ((HttpRequest) request)
                .addHeader(new Header("User-Agent", RandomUserAgent.getRandomUserAgent()));
    }

    @Override
    public void close() {
        torWorkers.forEach(TorWorker::close);
    }
}
