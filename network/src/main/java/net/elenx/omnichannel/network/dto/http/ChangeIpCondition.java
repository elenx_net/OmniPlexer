package net.elenx.omnichannel.network.dto.http;

import java.util.function.Predicate;

@FunctionalInterface
public interface ChangeIpCondition extends Predicate<OmniHttpResponse>
{
    ChangeIpCondition NEVER = omniHttpResponse -> false;
    ChangeIpCondition WHEN_429_CODE = omniHttpResponse -> omniHttpResponse.statusCode() == 429;
}
