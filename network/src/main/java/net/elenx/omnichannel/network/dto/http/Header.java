package net.elenx.omnichannel.network.dto.http;

public record Header(String key, String... value)
{
}
