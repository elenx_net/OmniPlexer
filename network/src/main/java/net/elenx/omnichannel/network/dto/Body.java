package net.elenx.omnichannel.network.dto;

import java.io.InputStream;

public interface Body
{
    String asText();

    InputStream asInputStream();

    <T> T convertTo(Class<T> typeClass);
}
