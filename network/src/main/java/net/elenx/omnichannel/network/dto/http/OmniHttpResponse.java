package net.elenx.omnichannel.network.dto.http;

import net.elenx.omnichannel.network.dto.OmniNetworkResponse;

import java.util.List;

public interface OmniHttpResponse extends OmniNetworkResponse
{
    int statusCode();

    List<Header> headers();
}
