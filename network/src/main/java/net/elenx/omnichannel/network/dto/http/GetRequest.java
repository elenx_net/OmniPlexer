package net.elenx.omnichannel.network.dto.http;

import lombok.Builder;
import lombok.Getter;

import java.util.List;
import java.util.Optional;

@Getter
public class GetRequest extends HttpRequest {
    @Builder
    private GetRequest(String url, List<Header> headers, ChangeIpCondition changeIpCondition) {
        super(url, headers, changeIpCondition);
    }

    @Override
    public HttpMethod httpMethod() {
        return HttpMethod.GET;
    }

    @Override
    public Optional<Object> body() {
        return Optional.empty();
    }

    @Override
    HttpRequest withNewHeaders(List<Header> newHeaders) {
        return new GetRequest(this.url, newHeaders, this.changeIpCondition);
    }
}
