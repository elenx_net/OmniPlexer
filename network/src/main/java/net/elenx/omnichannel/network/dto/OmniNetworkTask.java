package net.elenx.omnichannel.network.dto;

import java.util.function.Predicate;

public interface OmniNetworkTask
{
    Predicate<? extends OmniNetworkResponse> changeIpCondition();
}
