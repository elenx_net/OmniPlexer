package net.elenx.omnichannel.network.httpnetworkservice.webclient;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import net.elenx.omnichannel.network.httpnetworkservice.Proxy;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;
import reactor.netty.transport.ProxyProvider;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class WebClientFactory
{
    public static WebClient create(Proxy proxy)
    {
        HttpClient httpClient = HttpClient.create()
                .proxy(typeSpec -> configureProxy(typeSpec, proxy))
                .keepAlive(false);

        ReactorClientHttpConnector connector = new ReactorClientHttpConnector(httpClient);
        return WebClient.builder().clientConnector(connector).build();
    }

    public static WebClient create()
    {
        return WebClient.create();
    }

    private static void configureProxy(ProxyProvider.TypeSpec proxy, Proxy proxyConfig)
    {
        var proxyType = switch (proxyConfig.type())
                {
                    case SOCKS_5 -> ProxyProvider.Proxy.SOCKS5;
                };

        var proxyAddress = proxyConfig.proxyAddress();
        proxy.type(proxyType)
                .host(proxyAddress.getHostName())
                .port(proxyAddress.getPort())
                .connectTimeoutMillis(30000);
    }
}
