package net.elenx.omnichannel.network.httpnetworkservice.tor.configuration;

public record TorNode(String host, int port, int controlPort, String controlPortAuth)
{
}