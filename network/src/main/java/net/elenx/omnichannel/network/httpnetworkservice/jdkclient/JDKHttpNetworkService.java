package net.elenx.omnichannel.network.httpnetworkservice.jdkclient;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.experimental.FieldDefaults;
import net.elenx.omnichannel.network.dto.http.Header;
import net.elenx.omnichannel.network.dto.http.OmniHttpRequest;
import net.elenx.omnichannel.network.dto.http.OmniHttpResponse;
import net.elenx.omnichannel.network.httpnetworkservice.HttpNetworkService;
import net.elenx.omnichannel.network.httpnetworkservice.webclient.WebClientHttpResponse;
import net.elenx.omnichannel.network.httpnetworkservice.webclient.WebClientResponseBody;

import java.io.InputStream;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpHeaders;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
public class JDKHttpNetworkService implements HttpNetworkService {

    HttpClient httpClient;

    @Override
    @SneakyThrows
    public CompletableFuture<OmniHttpResponse> send(OmniHttpRequest request) {
        var builder = HttpRequest.newBuilder(new URI(request.url()))
                .method(request.httpMethod().toString(), request.body().map(body -> HttpRequest.BodyPublishers.ofString(body.toString())).orElse(HttpRequest.BodyPublishers.noBody()));
        request.headers().forEach(header -> Arrays.stream(header.value()).forEach(headerValue -> builder.header(header.key(), headerValue)));
        builder.timeout(Duration.of(10, ChronoUnit.SECONDS));

        return httpClient.sendAsync(builder.build(), HttpResponse.BodyHandlers.ofInputStream())
                .toCompletableFuture()
                .thenApply(this::toOmniResponse);
    }

    private OmniHttpResponse toOmniResponse(HttpResponse<InputStream> httpResponse) {
        var body = new WebClientResponseBody(httpResponse.body());
        return new WebClientHttpResponse(httpResponse.statusCode(), body, fetchHeaders(httpResponse.headers()));
    }

    private List<Header> fetchHeaders(HttpHeaders headers) {
        return headers.map().entrySet()
                .stream()
                .map(entry -> new Header(entry.getKey(), entry.getValue().toArray(String[]::new)))
                .toList();
    }
}
