package net.elenx.omnichannel.network.dto.http;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

@Getter
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PROTECTED)
public abstract class HttpRequest implements OmniHttpRequest {
    String url;
    List<Header> headers;
    ChangeIpCondition changeIpCondition;

    @Override
    public String url() {
        return url;
    }

    @Override
    public List<Header> headers() {
        return headers == null ? Collections.emptyList() : headers;
    }

    @Override
    public Predicate<OmniHttpResponse> changeIpCondition() {
        return changeIpCondition == null ? ChangeIpCondition.NEVER : changeIpCondition;
    }

    public HttpRequest addHeader(Header header) {
        var newHeaders = new ArrayList<Header>(headers.size() + 1);
        newHeaders.addAll(headers);
        newHeaders.add(header);
        return withNewHeaders(Collections.unmodifiableList(newHeaders));
    }

    abstract HttpRequest withNewHeaders(List<Header> newHeaders);
}
