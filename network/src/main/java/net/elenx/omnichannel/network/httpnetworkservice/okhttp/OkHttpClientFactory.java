package net.elenx.omnichannel.network.httpnetworkservice.okhttp;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import okhttp3.OkHttpClient;

import java.util.concurrent.TimeUnit;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class OkHttpClientFactory {

    @SneakyThrows
    public static OkHttpClient create() {
        return new OkHttpClient.Builder()
                .callTimeout(30, TimeUnit.SECONDS)
                .build();
    }
}
