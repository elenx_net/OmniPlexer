package net.elenx.omnichannel.network.httpnetworkservice.tor;

class TimestampSampler
{
    private long timestmap;

    void update()
    {
        this.timestmap = System.nanoTime();
    }

    boolean lastUpdateAfter(long timestamp)
    {
        return timestamp > this.timestmap;
    }

    public static long now()
    {
        return System.nanoTime();
    }
}
