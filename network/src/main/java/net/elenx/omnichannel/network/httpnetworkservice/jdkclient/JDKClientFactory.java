package net.elenx.omnichannel.network.httpnetworkservice.jdkclient;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.net.http.HttpClient;
import java.util.concurrent.Executors;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class JDKClientFactory {

    public static HttpClient create() {
        return HttpClient.newBuilder()
                .executor(Executors.newFixedThreadPool(5))
                .build();

    }
}
