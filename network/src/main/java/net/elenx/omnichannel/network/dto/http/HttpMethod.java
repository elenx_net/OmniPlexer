package net.elenx.omnichannel.network.dto.http;

public enum HttpMethod
{
    GET, POST, PUT, DELETE
}
