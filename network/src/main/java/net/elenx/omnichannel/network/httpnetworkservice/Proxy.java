package net.elenx.omnichannel.network.httpnetworkservice;

import java.net.InetSocketAddress;

public record Proxy(InetSocketAddress proxyAddress, Type type)
{

    public enum Type {
        SOCKS_5
    }
}
