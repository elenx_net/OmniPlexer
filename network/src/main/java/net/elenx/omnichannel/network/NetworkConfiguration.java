package net.elenx.omnichannel.network;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import net.elenx.omnichannel.network.httpnetworkservice.Proxy;
import net.elenx.omnichannel.network.httpnetworkservice.okhttp.OkHttpClientFactory;
import net.elenx.omnichannel.network.httpnetworkservice.okhttp.OkHttpClientNetworkService;
import net.elenx.omnichannel.network.httpnetworkservice.tor.TorHttpNetworkServiceFactory;
import net.elenx.omnichannel.network.httpnetworkservice.tor.configuration.TorNetworkServiceNode;
import net.elenx.omnichannel.network.httpnetworkservice.tor.configuration.TorNode;
import net.elenx.omnichannel.network.httpnetworkservice.webclient.WebClientFactory;
import net.elenx.omnichannel.network.httpnetworkservice.webclient.WebClientNetworkService;

import java.net.InetSocketAddress;
import java.util.List;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class NetworkConfiguration {

    public static NetworkService networkService() {
        var httpNetworkService = new OkHttpClientNetworkService(OkHttpClientFactory.create());
        return new NetworkFacade(httpNetworkService);
    }

    public static NetworkService networkService(List<TorNode> torNodes) {
        var httpNetworkService = torNodes.stream()
                .map(NetworkConfiguration::createTorNode)
                .collect(collectingAndThen(toList(), TorHttpNetworkServiceFactory::create));

        return new NetworkFacade(httpNetworkService);
    }

    private static TorNetworkServiceNode createTorNode(TorNode torNode) {
        var proxy = new Proxy(new InetSocketAddress(torNode.host(), torNode.port()), Proxy.Type.SOCKS_5);
        var webClient = WebClientFactory.create(proxy);
        var networkService = new WebClientNetworkService(webClient);
        return new TorNetworkServiceNode(torNode, networkService);
    }

}
