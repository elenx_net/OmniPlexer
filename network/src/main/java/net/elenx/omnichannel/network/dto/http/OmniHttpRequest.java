package net.elenx.omnichannel.network.dto.http;

import net.elenx.omnichannel.network.dto.OmniNetworkTask;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public interface OmniHttpRequest extends OmniNetworkTask {
    String url();

    HttpMethod httpMethod();

    List<Header> headers();

    Optional<Object> body();

    Predicate<OmniHttpResponse> changeIpCondition();
}
