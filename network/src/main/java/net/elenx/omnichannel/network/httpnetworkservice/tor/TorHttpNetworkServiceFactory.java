package net.elenx.omnichannel.network.httpnetworkservice.tor;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import net.elenx.omnichannel.network.httpnetworkservice.HttpNetworkService;
import net.elenx.omnichannel.network.httpnetworkservice.tor.configuration.TorNetworkServiceNode;

import java.util.List;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class TorHttpNetworkServiceFactory
{
    public static HttpNetworkService create(List<TorNetworkServiceNode> nodes)
    {
        var workers = nodes.stream()
                .map(node -> new TorWorker(node.networkService(), node.torNode()))
                .toList();
        return new TorHttpNetworkService(workers);
    }
}
