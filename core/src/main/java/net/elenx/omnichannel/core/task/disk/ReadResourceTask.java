package net.elenx.omnichannel.core.task.disk;

import java.nio.file.Path;

public class ReadResourceTask implements OmniDiskTask
{
    private final Path path;

    public ReadResourceTask(Path path)
    {
        this.path = path;
    }

    @Override
    public OmniDiskResponse execute()
    {
        System.out.println("Executing Read Resource Task " + path.toString());
        return new OmniDiskResponse();
    }
}
