package net.elenx.omnichannel.core.structures;

import java.util.HashMap;
import java.util.Map;

public class Maps
{
    public static <K, V> Map<K, V> hashMap(int size)
    {
        return new HashMap<>(size);
    }
}
