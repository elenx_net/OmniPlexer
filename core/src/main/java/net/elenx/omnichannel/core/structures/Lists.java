package net.elenx.omnichannel.core.structures;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Lists
{
    public static <K> List<K> arrayList(int size)
    {
        return new ArrayList<>(size);
    }

    public static <K> List<K> listOf(K element)
    {
        return Collections.singletonList(element);
    }
}
