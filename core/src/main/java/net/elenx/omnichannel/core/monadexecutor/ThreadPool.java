package net.elenx.omnichannel.core.monadexecutor;

import io.netty.util.concurrent.DefaultThreadFactory;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ThreadPool {

    public static ExecutorService nonBlockingNetworkPool() {
        var threads = Runtime.getRuntime().availableProcessors();
        var factory = new DefaultThreadFactory("NON-BLOCKING-NETWORK-POOL");
        var tp = new ThreadPoolExecutor(threads, threads, 60, TimeUnit.SECONDS, new LinkedBlockingQueue<>(), factory);
        tp.allowCoreThreadTimeOut(true);
        return tp;
    }
}
