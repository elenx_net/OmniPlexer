package net.elenx.omnichannel.core.monadexecutor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import net.elenx.omnichannel.core.monadexecutor.config.OmniConfig;
import net.elenx.omnichannel.network.NetworkConfiguration;
import net.elenx.omnichannel.network.NetworkService;
import net.elenx.omnichannel.network.httpnetworkservice.tor.configuration.TorNode;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Objects;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class MonadExecutorLoader
{
    private static final String CONFIG_FILE_PATH = "omni.yml";

    @SneakyThrows
    public static NetworkService networkService()
    {
        try (var configurationFileStream = MonadExecutorLoader.class.getClassLoader().getResourceAsStream(CONFIG_FILE_PATH))
        {
            var objectMapper = new ObjectMapper(new YAMLFactory());
            var config = objectMapper.readValue(configurationFileStream, OmniConfig.class);

            var networkServiceConfig = config.networkConfig();
            if (networkServiceConfig == null || !StringUtils.hasText(networkServiceConfig.type()))
            {
                return createWithDefaultNetworkService();
            } else
            {
                var type = networkServiceConfig.type();
                if (Objects.equals("TOR", type))
                {
                    return createWithTorConfiguration(networkServiceConfig.torNodes());
                } else
                {
                    throw new IllegalArgumentException("Unknown network-service type: " + type);
                }
            }
        }
    }

    private static NetworkService createWithDefaultNetworkService()
    {
        return NetworkConfiguration.networkService();
    }

    private static NetworkService createWithTorConfiguration(List<TorNode> torNodes)
    {
        if (torNodes == null || torNodes.isEmpty())
        {
            throw new IllegalArgumentException("At least one tor-node is required for TOR network service type");
        }
        return NetworkConfiguration.networkService(torNodes);
    }
}
