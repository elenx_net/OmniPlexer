package net.elenx.omnichannel.core.monad;

import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Scheduler;
import io.reactivex.rxjava3.schedulers.Schedulers;
import lombok.extern.slf4j.Slf4j;
import net.elenx.omnichannel.core.monadexecutor.MonadExecutorLoader;
import net.elenx.omnichannel.core.monadexecutor.ThreadPool;
import net.elenx.omnichannel.core.task.disk.OmniDiskResponse;
import net.elenx.omnichannel.core.task.disk.OmniDiskTask;
import net.elenx.omnichannel.network.NetworkService;
import net.elenx.omnichannel.network.dto.http.OmniHttpRequest;
import net.elenx.omnichannel.network.dto.http.OmniHttpResponse;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

@Slf4j
class OmniStep<T> implements Omni<T> {
    private static final Scheduler POOL = Schedulers.from(ThreadPool.nonBlockingNetworkPool());

    private static final NetworkService networkService = MonadExecutorLoader.networkService();

    Flowable<T> currentStep;

    OmniStep(Flowable<T> nextStep) {
        this.currentStep = nextStep;
    }

    @Override
    public Omni<OmniHttpResponse> mapNetwork(Function<? super T, OmniHttpRequest> mapper) {
        var nextStep = currentStep
                .observeOn(POOL)
                .flatMap(t -> Flowable.fromCompletionStage(networkService.send(mapper.apply(t))));

        return new OmniStep<>(nextStep);
    }

    @Override
    public Omni<OmniHttpResponse> mapNetworkMany(Function<? super T, Iterable<OmniHttpRequest>> mapper) {
        var nextStep = currentStep
                .observeOn(POOL)
                .flatMapIterable(mapper::apply)
                .flatMap(t -> Flowable.fromCompletionStage(networkService.send(t)));

        return new OmniStep<>(nextStep);
    }

    @Override
    public <R> Omni<R> map(Function<? super T, R> mapper) {
        return new OmniStep<>(currentStep.map(mapper::apply));
    }

    @Override
    public <R> Omni<R> mapMany(Function<? super T, Iterable<R>> mapper) {
        return new OmniStep<>(currentStep.flatMapIterable(mapper::apply));
    }

    @Override
    public <R> Omni<R> mapCpu(Function<? super T, R> mapper) {
        var nextStep = currentStep.map(mapper::apply);

        return new OmniStep<>(nextStep);
    }

    @Override
    public <R> Omni<R> mapCpuMany(Function<? super T, Iterable<R>> mapper) {
        var nextStep = currentStep.flatMapIterable(mapper::apply);

        return new OmniStep<>(nextStep);
    }

    @Override
    public Omni<OmniDiskResponse> mapDiskIO(Function<? super T, OmniDiskTask> mapper) {
        var nextStep = currentStep
                .observeOn(Schedulers.io())
                .map(t -> mapper.apply(t).execute());

        return new OmniStep<>(nextStep);
    }

    @Override
    public Omni<OmniDiskResponse> mapDiskIOMany(Function<? super T, Iterable<OmniDiskTask>> mapper) {
        var nextStep = currentStep
                .observeOn(Schedulers.io())
                .flatMapIterable(mapper::apply)
                .map(OmniDiskTask::execute);

        return new OmniStep<>(nextStep);
    }

    @Override
    public Omni<T> filter(Predicate<T> filter) {
        return new OmniStep<>(currentStep.filter(filter::test));
    }

    @Override
    public void forEach(Consumer<? super T> consumer) {
        currentStep.subscribe(consumer::accept, ex -> log.error("Error during processing elements.", ex));
    }

    @Override
    public Flowable<T> toFlowable() {
        return currentStep;
    }

    @SafeVarargs
    public static <T> Omni<T> from(T... elements) {
        return new OmniStep<>(Flowable.fromArray(elements));
    }

    public static <T> Omni<T> from(Stream<T> source) {
        return new OmniStep<>(Flowable.fromStream(source));
    }
}
