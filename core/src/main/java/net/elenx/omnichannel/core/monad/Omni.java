package net.elenx.omnichannel.core.monad;

import io.reactivex.rxjava3.core.Flowable;
import net.elenx.omnichannel.core.task.disk.OmniDiskResponse;
import net.elenx.omnichannel.core.task.disk.OmniDiskTask;
import net.elenx.omnichannel.network.dto.http.OmniHttpRequest;
import net.elenx.omnichannel.network.dto.http.OmniHttpResponse;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public interface Omni<T>
{
    Omni<OmniHttpResponse> mapNetwork(Function<? super T, OmniHttpRequest> mapper);

    Omni<OmniHttpResponse> mapNetworkMany(Function<? super T, Iterable<OmniHttpRequest>> mapper);

    <R> Omni<R> map(Function<? super T, R> mapper);

    <R> Omni<R> mapMany(Function<? super T, Iterable<R>> mapper);

    <R> Omni<R> mapCpu(Function<? super T, R> mapper);

    <R> Omni<R> mapCpuMany(Function<? super T, Iterable<R>> mapper);

    Omni<OmniDiskResponse> mapDiskIO(Function<? super T, OmniDiskTask> mapper);

    Omni<OmniDiskResponse> mapDiskIOMany(Function<? super T, Iterable<OmniDiskTask>> mapper);

    Omni<T> filter(Predicate<T> filter);

    @SafeVarargs
    static <T> Omni<T> from(T... source)
    {
        return OmniStep.from(source);
    }

    static <T> Omni<T> fromStream(Stream<T> source)
    {
        return OmniStep.from(source);
    }

    void forEach(Consumer<? super T> consumer);

    Flowable<T> toFlowable();
}
