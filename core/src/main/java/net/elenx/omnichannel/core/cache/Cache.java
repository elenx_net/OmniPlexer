package net.elenx.omnichannel.core.cache;

import java.util.Map;
import java.util.Optional;

public class Cache
{
    private final Map<Object, Object> cache;

    public Cache(Map<Object, Object> cache)
    {
        this.cache = cache;
    }

    public void put(Object key, Object value)
    {
        cache.put(key, value);
    }

    public Optional<Object> get(Object key)
    {
        return Optional.ofNullable(cache.get(key));
    }

    public void remove(Object key)
    {
        cache.remove(key);
    }
}
