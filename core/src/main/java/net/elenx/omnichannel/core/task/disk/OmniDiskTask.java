package net.elenx.omnichannel.core.task.disk;

import java.nio.file.Path;

public interface OmniDiskTask
{
    OmniDiskResponse execute();

    static OmniDiskTask readFile(Path path)
    {
        return new ReadResourceTask(path);
    }
}
