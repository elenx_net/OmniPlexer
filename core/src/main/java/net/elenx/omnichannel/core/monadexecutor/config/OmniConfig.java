package net.elenx.omnichannel.core.monadexecutor.config;

public record OmniConfig(NetworkConfig networkConfig)
{
}
