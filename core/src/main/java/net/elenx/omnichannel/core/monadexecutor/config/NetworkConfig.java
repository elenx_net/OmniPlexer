package net.elenx.omnichannel.core.monadexecutor.config;


import net.elenx.omnichannel.network.httpnetworkservice.tor.configuration.TorNode;

import java.util.List;

public record NetworkConfig(String type, List<TorNode> torNodes)
{
}
