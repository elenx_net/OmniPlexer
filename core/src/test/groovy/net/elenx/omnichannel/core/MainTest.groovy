package net.elenx.omnichannel.core

import spock.lang.Specification

class MainTest extends Specification
{

    def "testSpock"()
    {
        given:
          int x = 1
        when:
          int result = x + 1
        then:
          result == 2
    }
}
