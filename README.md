Zadaniem omnichannel jest dostarczenie wysokopoziomego API do wykonywania współbieżnie operacji z podziałem na networking, disk io oraz cpu w jak najbardziej efektywny sposób, włączając w to cache'owanie i serializacje na różnych poziomach abstrakcji.  
```java
.mapNetwork(s -> httpRequest) 
.mapCpu(httpResponse -> createMultipileRequests(httpResponse))
.mapNetwork(list<httpRequest> listaRequesow ->  listaRequestow)
.mapCpu(...)
.mapDiskIO(...)
```

**1. W czym OmniChannel jest lepszy od znanych bibliotek takich jak io.reactor lub RxJava?**  
Wyżej wymionie biblioteki, nie posiadają informacji, jakiego rodzaju zadanie (cpu / network / disk) chcemy wykonać, w związku z tym
to programista musi zadbać o dostarczenie odpowiedniej puli wątków i szeregowanie zadań, musi dostarczyć klienty protokołów http, 
websocket, rpc itp oraz oprogramować całą logike wysyłania i odbierania requestów. Korzystając z omnichannel przekazujemy tylko ```Request``` odpowiedniego typu, np ```HttpRequest```, ```WebsocketRequest``` lub ```CpuRequest``` i otrzymujemy w odpowiedzi ```Response```. Można ze sobą łączyć te narzędzia, ponieważ z założenia omnichannel nie będzię posiadał bogatego API, obsługi błędów i backpressure.  