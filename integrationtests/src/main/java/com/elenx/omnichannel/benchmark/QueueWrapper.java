package com.elenx.omnichannel.benchmark;

import com.lmax.disruptor.AlertException;
import com.lmax.disruptor.TimeoutException;

interface QueueWrapper
{
    Object take() throws InterruptedException, TimeoutException, AlertException;

    void put(final Object obj) throws InterruptedException;

    void stop();
}
