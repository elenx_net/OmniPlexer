package com.elenx.omnichannel.benchmark;

import com.lmax.disruptor.AlertException;
import com.lmax.disruptor.TimeoutException;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Group;
import org.openjdk.jmh.annotations.GroupThreads;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.TearDown;
import org.openjdk.jmh.annotations.Timeout;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.infra.Control;
import org.openjdk.jmh.profile.GCProfiler;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.time.Instant;
import java.util.concurrent.TimeUnit;

@Fork(value = 1)
@Warmup(iterations = 2, time = 30)
@Measurement(iterations = 2, time = 30)
@OutputTimeUnit(TimeUnit.MICROSECONDS)
@BenchmarkMode({Mode.Throughput})
@State(Scope.Benchmark)
@Timeout(time = 1, timeUnit = TimeUnit.NANOSECONDS)
public class QueueBenchmark
{
    private QueueWrapper queue;

    @Param
    public QueueProvider queueProvider;

    @Param({"8192"})
    public int queueCapacity;

    @Setup(Level.Iteration)
    public void setupQueue()
    {
        queue = queueProvider.provide(queueCapacity);
    }

    @TearDown(Level.Iteration)
    public void tear(final Control control)
    {
        control.stopMeasurement = true;
        queue.stop();
        queue = null;
    }

    @Group("queueBenchmark")
    @GroupThreads
    @Benchmark
    public void producer() throws InterruptedException
    {
        queue.put(new Object());
    }

    @Group("queueBenchmark")
    @GroupThreads
    @Benchmark
    public void consumer(final Blackhole bh) throws InterruptedException, TimeoutException, AlertException
    {
        bh.consume(queue.take());
    }

    public static void main(String[] args) throws RunnerException
    {
        Options options = new OptionsBuilder()
                .include(QueueBenchmark.class.getSimpleName())
                .addProfiler(GCProfiler.class)
                .output("QueuesBenchmark-" + Instant.now())
                .build();
        new Runner(options).run();
    }
}
