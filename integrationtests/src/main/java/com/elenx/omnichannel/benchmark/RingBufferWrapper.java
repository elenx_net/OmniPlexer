package com.elenx.omnichannel.benchmark;

import com.lmax.disruptor.AlertException;
import com.lmax.disruptor.BusySpinWaitStrategy;
import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.Sequence;
import com.lmax.disruptor.SequenceBarrier;
import com.lmax.disruptor.TimeoutException;
import com.lmax.disruptor.dsl.ProducerType;

class RingBufferWrapper implements QueueWrapper
{
    private final RingBuffer<Event> ringBuffer;
    private final Sequence consumerSequence;
    private final SequenceBarrier ringSequenceBarrier;

    RingBufferWrapper(final int capacity)
    {
        consumerSequence = new Sequence();
        ringBuffer = RingBuffer.create(
                ProducerType.SINGLE,
                Event::new,
                capacity,
                new BusySpinWaitStrategy());
        ringBuffer.addGatingSequences(consumerSequence);
        ringSequenceBarrier = ringBuffer.newBarrier();
    }

    @Override
    public Object take() throws InterruptedException, TimeoutException, AlertException
    {
        final long sequence = consumerSequence.incrementAndGet();
        ringSequenceBarrier.waitFor(sequence);
        return ringBuffer.get(sequence).getPayload();
    }

    @Override
    public void put(final Object obj)
    {
        ringBuffer.publishEvent((event, sequence) -> event.setPayload(obj));
    }

    @Override
    public void stop()
    {
        consumerSequence.incrementAndGet();
    }

    private static final class Event
    {
        private Object payload;

        public Object getPayload()
        {
            return payload;
        }

        public void setPayload(final Object payload)
        {
            this.payload = payload;
        }
    }
}
