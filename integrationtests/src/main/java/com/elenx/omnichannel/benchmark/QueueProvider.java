package com.elenx.omnichannel.benchmark;

import org.jctools.queues.SpscArrayQueue;
import org.jctools.queues.SpscChunkedArrayQueue;
import org.jctools.queues.SpscLinkedQueue;
import org.jctools.queues.SpscUnboundedArrayQueue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;

public enum QueueProvider
{
    JDK_ARRAY_BLOCKING_QUEUE
            {
                @Override
                QueueWrapper provide(final int capacity)
                {
                    return new BlockingQueueWrapper(new ArrayBlockingQueue<>(capacity));
                }
            },

    JDK_LINKED_BLOCKING_QUEUE
            {
                @Override
                QueueWrapper provide(final int capacity)
                {
                    return new BlockingQueueWrapper(new LinkedBlockingQueue<>(capacity));
                }
            },

    JDK_LINKED_BLOCKING_DEQUE
            {
                @Override
                QueueWrapper provide(final int capacity)
                {
                    return new BlockingQueueWrapper(new LinkedBlockingDeque<>(capacity));
                }
            },

    JDK_CONCURRENT_LINKED_QUEUE
            {
                @Override
                QueueWrapper provide(final int capacity)
                {
                    return new SpinWaitingQueueWrapper(new ConcurrentLinkedQueue<>());
                }
            },

    JDK_CONCURRENT_LINKED_DEQUE
            {
                @Override
                QueueWrapper provide(final int capacity)
                {
                    return new SpinWaitingQueueWrapper(new ConcurrentLinkedDeque<>());
                }
            },

    JCTOOLS_SPSC_ARRAY_QUEUE
            {
                @Override
                QueueWrapper provide(final int capacity)
                {
                    return new SpinWaitingQueueWrapper(new SpscArrayQueue<>(capacity));
                }
            },

    JCTOOLS_SPSC_CHUNKED_ARRAY_QUEUE
            {
                @Override
                QueueWrapper provide(final int capacity)
                {
                    return new SpinWaitingQueueWrapper(new SpscChunkedArrayQueue<>(capacity));
                }
            },

    JCTOOLS_SPSC_UNBOUNDED_ARRAY_QUEUE
            {
                @Override
                QueueWrapper provide(final int capacity)
                {
                    return new SpinWaitingQueueWrapper(new SpscUnboundedArrayQueue<>(capacity));
                }
            },

    JCTOOLS_SPSC_LINKED_QUEUE
            {
                @Override
                QueueWrapper provide(final int capacity)
                {
                    return new SpinWaitingQueueWrapper(new SpscLinkedQueue<>());
                }
            },

    DISTRUPTOR
            {
                @Override
                QueueWrapper provide(final int capacity)
                {
                    return new RingBufferWrapper(capacity);
                }
            };

    abstract QueueWrapper provide(final int capacity);
}
