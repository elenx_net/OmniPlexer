package com.elenx.omnichannel.benchmark;

import java.util.Queue;

class SpinWaitingQueueWrapper implements QueueWrapper
{
    private final Queue<Object> queue;

    SpinWaitingQueueWrapper(final Queue<Object> queue)
    {
        this.queue = queue;
    }

    @Override
    public Object take()
    {
        Object element = null;
        while (element == null)
        {
            element = queue.poll();
        }
        return element;
    }

    @Override
    public void put(final Object obj)
    {
        boolean result = false;
        while (!result)
        {
            result = queue.offer(obj);
        }
    }

    @Override
    public void stop()
    {
        if (queue.isEmpty())
        {
            queue.add(new Object());
        } else
        {
            queue.remove();
        }
    }
}
