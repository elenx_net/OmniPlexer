package com.elenx.omnichannel.benchmark;

import java.util.concurrent.BlockingQueue;

@SuppressWarnings("squid:S899")
class BlockingQueueWrapper implements QueueWrapper
{
    private final BlockingQueue<Object> queue;

    BlockingQueueWrapper(final BlockingQueue<Object> queue)
    {
        this.queue = queue;
    }

    @Override
    public Object take() throws InterruptedException
    {
        return queue.take();
    }

    @Override
    public void put(final Object obj) throws InterruptedException
    {
        queue.put(obj);
    }

    @Override
    public void stop()
    {
        if (queue.isEmpty())
        {
            queue.add(new Object());
        } else
        {
            queue.remove();
        }
    }
}
